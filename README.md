# Prueba de practica

En este proyecto se realiza lo solicitado, usando el lenguaje JavaScript para reversar
los caracteres con la funcion darVuelta. Para ejecutar el proyecto, se requiere instalar 
Node.js y para ejecutar en el terminar se ingresa node prueba_reversar.js


El metodo split() separa los caracteres del objeto.

El metodo reverse() se encarga a invertir el objeto.

El metodo join() se encarga a juntar los caracteres del objeto. 

Con console.log() muestra el resultado en la consola.
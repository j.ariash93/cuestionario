// Invertir caracteres
function darVuelta(valor) {
    return String(valor).split('').reverse().join('')
}
// Invertir Palabras
function darVueltaPalabras(valor) {
    return String(valor).split(' ').reverse().join(' ')
}

//Resultado caracteres invertidos
console.log(darVuelta('hola mundo'))
console.log(darVuelta('hola mundo2'))
//Resultado palabras invertidas
console.log(darVueltaPalabras('Hola Mundo'))